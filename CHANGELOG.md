CHANGELOG
=========

1.0.1 (2015-03-22)
------------------

* removed redundant closing php tag from all files

1.0.0 (2015-03-17)
------------------

* initial release