HipChat Notification API Wrapper for Laravel
============================================

A HipChat API wrapper for Laravel 5.x using Guzzle, which implements a simple room notification interface using the 
HipChat v2 API and is designed to be used with room authorization tokens.

By [Simon Hampel](http://hampelgroup.com/).

This package provides a simple Laravel service provider and facade for our base HipChat notification API wrapper package
[hampel/hipchat-notify](https://bitbucket.org/hampel/hipchat-notify) - please refer to the documentation about that
package for instructions on how to use this API wrapper.

Installation
------------

The recommended way of installing the HipChat Notify package is through [Composer](http://getcomposer.org):

Require the package via Composer in your `composer.json`

    :::json
    {
        "require": {
            "hampel/hipchat-notify-laravel": "~1.0"
        }
    }

Run Composer to update the new requirement.

    :::bash
    $ composer update

The package is built to work with the Laravel 5 Framework.

Open your Laravel config file `app/config/app.php` and add the service provider in the `$providers` array:

    :::php
    'providers' => array(

        ...

        'HipChat\HipChatServiceProvider'

    ),

You may also optionally add an alias entry to the `$aliases` array in the same file for the HipChat facade:

	:::php
    "aliases" => array(

    	...

    	'HipChat'			  => 'HipChat\Facades\HipChat',

    ),

Finally, to utilise the HipChat API, you must generate an auth token using the HipChat admin control panel and then 
specify that token and your room ID in your `.env` file:

    :::bash
    HIPCHAT_ROOM_AUTH_TOKEN=your_hipchat_auth_token
    HIPCHAT_ROOM_ID=your_hipchat_room_id

Usage
-----

Use Laravel's App facade to gain access to the service provider in your code:

    :::php
    use HipChat\Message;

    $hipchat = App::make('hipchat');
    $hipchat->send(Message::createText('my notification message'));

... or just use the Facade instead:

    :::php
    HipChat::send(Message::createText('my notification message'));

Refer to the usage examples and code in the
[HipChat Notifier](https://bitbucket.org/hampel/hipchat-notify) repository for more details about how
to use the library.
