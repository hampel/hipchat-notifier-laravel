<?php namespace HipChat;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class HipChatServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{

	}

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->defineConfiguration();

		$this->app->bindShared('hipchat', function($app)
		{
			$auth_token = $app['config']->get('hipchat.auth_token');
			$client = new Client(Notifier::getConfig());

			return new Notifier($client, $auth_token);
		});

	}

	protected function defineConfiguration()
	{
		$this->mergeConfigFrom(
			__DIR__ . '/config/hipchat.php', 'hipchat'
		);
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('hipchat');
	}
}
