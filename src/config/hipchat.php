<?php
/**
 * Configuration for HipChat
 */

return array(

    /*
    |--------------------------------------------------------------------------
    | HipChat Room Authorization Token
    |--------------------------------------------------------------------------
    |
    | Specify the Authorization Token for your HipChat room
    |
    */

    'auth_token' => env('HIPCHAT_ROOM_AUTH_TOKEN', ''),

    /*
    |--------------------------------------------------------------------------
    | Default HipChat room for notifications
    |--------------------------------------------------------------------------
    |
    | Specify the room ID for notifications - must correspond to the room the
    | above auth token belongs to
    |
    */

    'room_id' => env('HIPCHAT_ROOM_ID', ''),

);
